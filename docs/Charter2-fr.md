# Charte des CHATONS

## Le Collectif

CHATONS est un Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires. Chaque membre de ce collectif, ci-après dénommé « CHATON », s’engage à respecter la présente charte. 
Ce qui n’est pas interdit explicitement par la présente charte ou par la loi, est autorisé. 
Le CHATON peut être de n’importe quelle forme juridique : personne morale ou personne physique (un particulier, une entreprise, une association, une SCOP, etc.).

Afin d'être au maximum inclusif, tout en ménageant différentes sensibilités sur le sujet, le collectif CHATONS a rédigé cette charte en appliquant les règles suivantes de l'écriture inclusive :
- l’accord en genre des noms de fonctions, grades, métiers et titres ;
- l’utilisation du féminin et du masculin quand on parle d’un groupe de personnes dans la mesure où cela n’alourdit pas trop la phrase (exemple : «  seules et seuls ») ;
- l’utilisation de la double flexion (exemple : « les candidates et candidats ») ;
- l’utilisation de la règle de proximité et/ou du nombre pour les accords.
Si des personnes souhaitaient aller plus loin encore dans l'inclusivité et seraient volontaires pour maintenir une version de la charte avec une écriture différente, nous serions ravies et ravis d'accueillir vos contributions.
Vous pouvez aussi nous faire part de vos remarques sur ce sujet dans notre [forum](https://forum.chatons.org/t/revision-de-la-charte-ecriture-inclusive/).

>  **Critères requis :**
> - le CHATON s’engage à agir dans le respect et la bienveillance vis-à-vis des autres membres du collectif ;
> - le CHATON s’engage à agir dans le respect et la bienveillance vis-à-vis des utilisateurs et utilisatrices de ses services.

## Hébergeurs

Un hébergeur est ici défini comme une entité hébergeant des données fournies par des tiers et proposant des services par lesquels transitent ou sont stockées ces données. 
Les utilisateurs et utilisatrices ci-après dénommées « hébergées » sont les personnes, physiques ou morales, ayant la possibilité de créer ou modifier des données sur l’infrastructure du CHATON.

>  **Critères requis :**
> - le CHATON doit avoir les accès administrateur (accès root ) sur le système d’exploitation faisant fonctionner les services en ligne finaux ;
> - en cas de location de serveur (virtuels ou dédiés), le CHATON doit s’assurer que le fournisseur s’engage contractuellement à ne pas accéder aux données ;
> - le CHATON doit informer ses visiteurs et visiteuses, utilisateurs et utilisatrices du degré de contrôle qu’il a sur son infrastructure ;
> - le CHATON s’engage à afficher publiquement et sans ambiguïté son niveau de contrôle sur le matériel et les logiciels hébergeant les services et les données associées. Notamment, le nom du fournisseur ou prestataire d’hébergement physique des serveurs devra clairement être indiqué aux hébergées ;
> - le CHATON s’engage à ne pas utiliser des services de fournisseurs - informatiques ou non - dont les pratiques sont incompatibles avec les principes de la présente charte, notamment en termes de préservation et de captation des données privées. Le CHATON en informe ses utilisateurs et utilisatrices.
>
> **Critère recommandé :**
> - le CHATON doit avoir le contrôle total de son infrastructure technique (serveurs et réseau) des logiciels et des données associées (accès root, hyperviseur compris). Il doit être le seul à pouvoir accéder techniquement aux données.

## Alternatifs

En proposant des services libres en ligne, le CHATON se propose d’être une alternative au modèle des entreprises proposant des services fermés et centralisés à des fins de monopole et d’usage dévoyé des données personnelles.
Ces alternatives illustrent la diversité des solutions libres disponibles et pouvant être mises à disposition à des fins personnelles ou collectives. 

>  **Critères requis :**
> - le CHATON s’engage à publier la documentation de son infrastructure et de ses services ;
> - le CHATON s’engage à ne pas mettre en place de dispositif de suivi des usages des services proposés autres qu’à des fins statistiques ou administratives ;
> - le CHATON s’engage à prioriser les libertés fondamentales de ses utilisateurs et utilisatrices, notamment le respect de leur vie privée, dans chacune de ses actions ;
> - le CHATON ne devra en aucun cas utiliser les services de régies publicitaires. Le sponsoring ou le mécénat, par le biais d’affichage de l’identité de structures partenaires (nom, logo, etc.) est autorisé, à condition qu’aucune information à caractère personnel ne soit communiquée aux partenaires ;
> - le CHATON ne devra faire aucune exploitation commerciale des données ou métadonnées des hébergées.


## Transparents

Le CHATON assure aux hébergées qu’aucun usage déloyal ne sera fait de leurs données, de leur identité ou de leurs droits, notamment par le biais de conditions générales d’utilisation claires, concises et facilement accessibles.
C’est en connaissance de cause que les utilisateurs et utilisatrices pourront user des services disponibles et prendre connaissance de leurs fonctionnalités, de leurs avantages, de leurs limites et de leurs emplois. 
Aucune donnée produite à l’aide des services n’appartient au CHATON. Le CHATON n’y appose aucun droit et n’y pratique aucune censure tant que les contenus respectent les Conditions Générales d’Utilisation.

>  **Critères requis :**
> - le CHATON s’engage à être transparent sur son infrastructure technique (distribution et logiciels utilisés). Pour des raisons de sécurité, certaines informations (numéro de version de logiciel, par exemple) pourront ne pas être communiquées publiquement ;
> - le CHATON s’engage à faire de son mieux (obligation de moyens) en terme de sécurisation de son infrastructure ;
> - le CHATON s’engage à mettre en place la politique de sauvegarde qui lui parait la plus adaptée et à la documenter publiquement ;
> - le CHATON s’engage à publier un journal des incidents techniques des services ; 
> - le CHATON s’engage à ne s’arroger aucun droit de propriété des contenus, données et métadonnées produits par les hébergées ou les utilisateurs et utilisatrices ;
> - le CHATON s’engage à indiquer publiquement quelle est son offre de service, ainsi que les tarifs associés ;
> - le CHATON s’engage à publier des Conditions Générales d’Utilisations (CGU) visibles, claires, non ambigües, et compréhensibles par le plus grand nombre ;
> - le CHATON s’engage à publier, dans ses CGU, une clause « Données personnelles et respect de la vie privée » indiquant clairement quelle est la politique du CHATON concernant les pratiques visées ;
> - le CHATON s’engage à rendre publics ses rapports d’activités, au moins pour la partie concernant son activité de CHATON ;
> - le CHATON s’engage à porter à la connaissance des hébergées les principales informations concernant sa politique de sécurité et de sauvegarde (en veillant évidemment à ce que ces informations ne portent pas atteinte à ladite politique de sécurité) ;
> - le CHATON s’engage à communiquer avec les hébergées sur les difficultés qu’il rencontre dans l’exploitation de sa structure et des différents services qu’il met à disposition, notamment à travers la mise en place de services de support et d'incidents.
> 
> **Critères recommandés :**
> - le CHATON s’engage à publier des statistiques d’usage anonymisées ;
> - le CHATON s’engage à rendre publics ses comptes, au moins pour la partie concernant son activité de CHATON.

## Ouverts 

L’utilisation des logiciels libres et des standards ouverts sur Internet est le moyen exclusif par lequel les membres proposent leurs services. L’accès au code source est au fondement des principes du Libre. Pour chaque service qu’il propose, le CHATON s’engage à utiliser des logiciels ou des éléments de code placés exclusivement sous licence libre.
En cas d’amélioration du code des logiciels utilisés, le CHATON s’engage à placer ses contributions sous licence libre (compatible avec la licence initiale) et encouragera toute contribution volontaire de la part des utilisateurs et utilisatrices en les invitant à contacter les auteurs et auteures. Le CHATON s’engage à rendre accessible le code source soit en publiant un lien vers le site officiel de l’application, soit, si ce dernier n’est plus disponible, en publiant le code utilisé.

>  **Critères requis :**
> - le CHATON s’engage à utiliser exclusivement des logiciels soumis à des licences libres, au sens de la Free Software Foundation, qu’elles soient compatibles ou non avec la licence GPL. Concernant les microcodes matériels pour lesquels il n’y a pas d’alternatives libres fonctionnelles, les logiciels d’amorçages (BIOS), ainsi que tout composant matériel ou logiciel auquel le CHATON n’a pas accès, ce dernier s’engage à en diffuser publiquement la liste, ainsi que leur objet.
> - le CHATON s’engage à utiliser exclusivement des distributions libres (GNU/Linux, FreeBSD, etc.) comme système d’exploitation pour l’infrastructure des hébergées ;
> - le CHATON s'engage, sur demande, à fournir la liste des paquets installés sur les serveurs hébergeant les services fournis aux utilisateurs et utilisatrices ;
> - le CHATON s’engage à n’utiliser que des formats ouverts dans l'exercice de son activité d'hébergement ;
> - le CHATON s’engage, s'il modifie le code source des logiciels utilisés, à rendre publiques ces modifications ;
> - le CHATON s’engage à contribuer, techniquement ou financièrement et dans la mesure de ses capacités, au mouvement du logiciel libre ; 
> - le CHATON s’engage à faciliter la possibilité pour les hébergées à quitter ses services avec les données associées dans des formats ouverts ;
> - le CHATON s’engage à supprimer définitivement toute information (comptes et données personnelles) concernant l’hébergé à la demande de ce dernier dans la limite des obligations légales et techniques en le lui indiquant au préalable (dans les CGU).
> 
> **Critères recommandés :**
> - Pour les logiciels liés à du matériel (BIOS, firmware, pilotes, etc.), le CHATON s’engage à utiliser exclusivement ceux soumis à des licences libres ;
> - le CHATON s'engage à lister publiquement ses contributions au Libre.


## Neutres

L’éthique du logiciel libre est faite de partage et d’indépendance. Le CHATON s’engage à ne pratiquer aucune censure a priori des contenus, aucune surveillance des actions des utilisateurs et utilisatrices, et à ne répondre à aucune demande administrative ou d’autorité sans une requête légale présentée en bonne et due forme.
L’égalité de l’accès à ces applications est un engagement fort : qu’elles soient payantes ou gratuites les offres des CHATONS doivent se faire sans discrimination technique ou sociale, et offrir toutes les garanties de la neutralité concernant la circulation des données.
Le respect de la vie privée des utilisateurs et utilisatrices est un besoin impérieux. Aucune donnée personnelle ne sera exploitée à des fins commerciales, transmise à des tiers ou utilisée à des fins non prévues par la présente charte, excepté pour des besoins statistiques et toujours dans le respect du cadre légal.

>  **Critères requis :**
> - le CHATON s’engage à ne pratiquer aucune surveillance des actions des utilisateurs et utilisatrices, autre qu'à des fins administratives, techniques, ou d'améliorations internes des services ;
> - le CHATON s’engage à ne pratiquer aucune censure a priori des contenus des hébergées ;
> - le CHATON s’engage à protéger au mieux les données de ses utilisateurs et utilisatrices des atteintes extérieures, notamment en utilisant autant que possible un chiffrement fort des données, lors de leurs réception/transmission sur le réseau (SSL/TLS) ou de leur stockage (fichiers ou base de données, notamment) ;
> - le CHATON s’engage à tenir une liste, à destination de ses hébergées, des requêtes administratives ou d’autorité qui lui auraient été présentées. Dans le cas d'une impossibilité légale de communiquer ces informations, le CHATON est encouragé à mettre en place en amont un dispositif de contournement type Warrant Canary ;
> - le CHATON s’engage à ne pas répondre à une requête administrative ou d’autorité nécessitant la communication d’informations personnelles avant que ne lui soit présentée une requête légale en bonne et dûe forme ;
> - le CHATON s’engage, si une action de modération s’avère nécessaire, à appliquer ses CGU avec bienveillance.


## Solidaires

Dans une démarche de partage et d’entraide, chaque CHATON diffuse à destination des autres CHATONS et du public un maximum de connaissances pour promouvoir les usages des logiciels libres et apprendre à installer des services libres en ligne. Ce partage des ressources, techniques et cognitives, fait d’Internet un bien commun, disponible pour toutes et tous et n’appartenant à personne.

>  **Critères requis :**
> - le CHATON s’engage à prévoir structurellement la possibilité de rencontrer physiquement, d'échanger, et de faire participer ses hébergées, dans la limite de ses moyens ;
> - le CHATON s’engage à avoir une démarche active et volontaire en terme d’accès pour toutes et tous aux services proposés, notamment en respectant les normes d’accessibilité web ;
> - le CHATON s'engage à mettre en œuvre et à promouvoir une forme d'organisation et de gouvernance inclusive, capable de s’adapter aux différences et à valoriser la diversité en veillant à ce que les groupes marginalisés ou exclus soient parties prenantes dans les processus de développement ;
> - le CHATON s’engage à ne pas exclure a priori de potentiels utilisateurs et utilisatrices aux services proposés. Le CHATON pourra définir des « publics cibles » auquel il souhaite s’adresser (par exemple sur des critères de proximité géographique, ou de centres d’intérêts). Cependant il devra, autant que possible, répondre aux demandes non pertinentes, par exemple en les orientant vers un autre CHATON ou en facilitant l’émergence de structures qui répondraient aux besoins non-satisfaits ;
> - le CHATON s’engage à définir un modèle économique basé sur la solidarité. En cas de services payants, ceux-ci devront être raisonnables et en adéquation avec les coûts de mise en œuvre. Par ailleurs le salaire – en équivalent temps plein, primes et dividendes compris – le plus faible de la structure ne saurait être inférieur à quatre fois le salaire – en équivalent temps plein, primes et dividendes compris – le plus élevé de la structure ;
> - le CHATON s’engage à diffuser le plus largement possible, publiquement et sous licence libre, ses connaissances. Une attention particulière sera apportée aux « non-techniciens et non-techniciennes » afin de leur permettre d’accroître leurs savoirs et leurs compétences et de pouvoir jouer ainsi pleinement leur rôle de contributeurs et contributrices ;
> - le CHATON s’engage à faciliter l’émancipation des publics qu’il souhaite toucher, notamment au travers de démarches d’éducation populaire (évènements, rencontres, formations internes ou externes, ateliers, temps de rédaction collaborative de documentations, etc.) afin de s’assurer qu’Internet demeure une technologie abordable par tout le monde.

